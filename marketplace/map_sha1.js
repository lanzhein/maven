/*
 *  Updates the sha1 of supported fabric mods
 *  Made by Deftware
 */

const https = require('https');
const crypto = require('crypto')
const fs = require('fs');

const sha1 = path => new Promise((resolve, reject) => {
    const hash = crypto.createHash('sha1')
    const rs = fs.createReadStream(path)
    rs.on('error', reject)
    rs.on('data', chunk => hash.update(chunk))
    rs.on('end', () => resolve(hash.digest('hex')))
});

(async () => {
    // Create temp dir
    if (!fs.existsSync("./temp/")) {
        fs.mkdirSync("./temp/");
    }
    for (let dir of fs.readdirSync("./plugins/")) {
        let pluginJson = JSON.parse(fs.readFileSync(`./plugins/${dir}/${dir}.json`));
        // Make sure its a fabric mod
        if (pluginJson["fabric"]) {
            for (let version of Object.keys(pluginJson["fabric"]["versions"])) {
                // Make sure it supports sha1
                if (pluginJson["fabric"]["versions"][version]["sha1"] !== undefined) {
                    // Delete old file
                    if (fs.existsSync(`./temp/${pluginJson.id}_${version}.jar`)) {
                       // fs.unlinkSync(`./temp/${pluginJson.id}_${version}.jar`);
                       continue;
                    }
                    // Download plugin
                    process.stdout.write(`Downloading plugin ${pluginJson.id} (${version})... `);
                    await new Promise((resolve, reject) => {
                        download(pluginJson["fabric"]["versions"][version]["url"], `./temp/${pluginJson.id}_${version}.jar`, resolve, reject);
                    });
                    process.stdout.write(`Done\n`);
                    await sleep(500);
                    // Get sha1
                    process.stdout.write(`Getting sha1... `);
                    let checksum = await sha1(`./temp/${pluginJson.id}_${version}.jar`);
                    process.stdout.write(`${checksum}\n`);
                    pluginJson["fabric"]["versions"][version]["sha1"] = checksum;
                    console.log(pluginJson["fabric"]["versions"][version]["sha1"])
                    // Sleep to avoid being throttled
                    await sleep(500);
                }
            }
            fs.writeFileSync(`./plugins/${dir}/${dir}.json`, JSON.stringify(pluginJson, null, 2));
        }
    }
    console.log("Done!");
})();

function sleep(ms) {
    return new Promise((resolve, reject) => {
        setTimeout(resolve, ms);
    });
}

/**
    Modified version of https://stackoverflow.com/questions/11944932/how-to-download-a-file-with-node-js-without-using-third-party-libraries
 */
function download(url, dest, resolve, reject) {
    const file = fs.createWriteStream(dest, {
        flags: "wx"
    });

    const request = https.get(url, {
        headers: {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36 Edge/12.246'
        }
    }, response => {
        if (response.statusCode === 200) {
            response.pipe(file);
        } else {
            file.close();
            fs.unlink(dest, () => {}); // Delete temp file
            if (response.statusCode === 301 || response.statusCode === 302) {
                return download(response.headers.location, dest, resolve, reject);
            }
            reject(`Server responded with ${response.statusCode}: ${response.statusMessage}`);
        }
    });

    request.on("error", err => {
        file.close();
        fs.unlink(dest, () => {}); // Delete temp file
        reject(err.message);
    });

    file.on("finish", () => {
        resolve();
    });

    file.on("error", err => {
        file.close();

        if (err.code === "EEXIST") {
            reject("File already exists");
        } else {
            fs.unlink(dest, () => {}); // Delete temp file
            reject(err.message);
        }
    });
}
